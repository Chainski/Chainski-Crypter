<a href="https://github.com/Chainski/Chainski-Crypter/blob/master/LICENSE"><img src="https://img.shields.io/github/license/Chainski/Chainski-Crypter"></a> 
<a href="https://github.com/chainski/Chainski-Crypter"><img src="https://img.shields.io/github/repo-size/Chainski/Chainski-Crypter?style=plastic"></a>
<a href="https://github.com/chainski/IPLogger"><img src="https://img.shields.io/badge/dynamic/json?label=Visitors&query=value&url=https%3A%2F%2Fapi.countapi.xyz%2Fhit%2FChainski%2FChainski-Crypter"></a> 
<a href="https://github.com/chainski/Chainski-Crypter"><img src="https://img.shields.io/badge/contributions-welcome-green"></a>

<p align="center">
<img src="https://user-images.githubusercontent.com/96607632/185758410-2b285b0b-59e1-4b5c-b6e0-1f269a24a7b8.png", width="400", height="400">
</p>


### Chainski Crypter 
An obfuscation tool for .Net + Native files.

### Main Features
- .NET - Coded in C#, required framework 4.0 dependency.
- Injection - Hide payload behind a legit process

## Added Features 
- Changed Injection Techniques
- Better UI
- More features coming soon

### Prerequisites

To open project you need:
- Visual Studio 2019+
- This repository

### DISCLAIMER !!! 

**This tool is for educational use only, the author will not be held responsible for any misuse of this tool.**

### Support and Contributions
My software is open source and free for public use. 
If you found any of these repos useful and would like to support this project financially, 
feel free to donate to my bitcoin address.

<a href="https://www.blockchain.com/btc/address/16T1fUehoGR4E2sj98u9e9mKuQ7uSLvxRJ"><img src="https://img.shields.io/badge/bitcoin-donate-yellow.svg"></a>


## Credits
https://github.com/NYAN-x-CAT/Lime-Crypter
